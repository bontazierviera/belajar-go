package v1

import (
	"../../controllers"
	"github.com/gin-gonic/gin"
)

//RoutesUser is a function of user routes
func RoutesUser(router *gin.Engine, parentRoute *gin.RouterGroup) *gin.Engine {

	//define controller
	userController := new(controllers.UserController)

	user := parentRoute.Group("user")
	{
		user.POST("/add", userController.CreateUser)
		user.GET("/get", userController.GetUserByUserName)
		user.PUT("/update", userController.UpdateUser)
	}

	return router
}
