package databases

import (
	"github.com/jinzhu/gorm"
)

var db *gorm.DB

//Init is a function initial to open db
func Init() {
	var err error
	db, err = gorm.Open("mysql", "root:@/db_go")
	if err != nil {
		panic("failed to connect database")
	}

	db.SingularTable(true)

	//Migrate the schema
	// db.AutoMigrate(&todoModel{})
}

//GetDB is a function to get db configuration
func GetDB() *gorm.DB {
	return db
}
