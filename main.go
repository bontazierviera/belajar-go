package main

import (
	"./databases"
	"./routes"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	databases.Init()
	r := routes.RoutesMain()

	r.Run(":9090") // listen and serve on 0.0.0.0:8080
}
